package com.mlcomponents.spiderman.core;

import com.mlcomponents.spiderman.network.DownloadManager;
import com.mlcomponents.spiderman.targets.Target;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The actual spider.
 * @author antonin
 */
public class Spider implements Runnable {

    private final DownloadManager downloadManager;
    private final Target target;
    private final OnlyOnceQueue<Link> links;

    /**
     *
     * @param downloadManager
     * @param target
     * @param links
     */
    public Spider(DownloadManager downloadManager, Target target, OnlyOnceQueue links) {
        this.downloadManager = downloadManager;
        this.target = target;
        this.links = links;
    }

    /**
     * The spider program. It's very simple, and must stay simple.
     */
    @Override
    public void run() {
        links.enqueue(target.getStartLink());

        //While there are links to process
        Link link;
        while ((link = links.dequeue()) != null) {
            try {
                //Download the current document
                downloadManager.downloadDocument(link.getDocument(), link.getURI());
                System.out.println(link.getURI().toString() + " -> " + link.getDocument().getLocalPath());

                //Extract new links
                LinkedList<Link> newLinks = link.extractLinks(link.getDocument());
                link.getDocument().setData(null);//Unload the data to avoid heap overflow
                //TODO / WARNING : synchronize access to document data for threads
                
                //Shuffle the links
                //Collections.shuffle(newLinks);
                
                //Enqueue the links
                links.enqueue(newLinks);
                
                //TODO : Enqueue the documents for the parser
                //Use a monitor for thread synchronisation
            } catch (IOException ex) {
                Logger.getLogger(Spider.class.getName()).log(Level.SEVERE, "Error while downloading file", ex);
                //discard the link for the moment
                //Todo : enqueue the link forcibly
            }

        }
    }

}
