/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlcomponents.spiderman.core;

/**
 * The main class to handle parsing tasks
 * @author antonin
 */
public class Parser {
    /**
     * This class is meant to contain what is going to be done by a parser.
     * It should be as most generic as the Spider, and be programmatically very similar to it.
     * Basically, the parser should run through a queue of documents made by the spider,
     * parse all the data, extract it to the good format, then wait for the parser to provide more data.
     * The spider/parser should be able to work in 2 separate threads, and have safe synchronisation
     * methods (a monitor with a "newDataToParse" condition for example).
     * The meeting point for the spider and the parser in term of data will be SpiderContext.
     * The idea is to allow the scraper to re-parse all its downloaded data,
     * using the refresh function of queues for example, without even slowing down the spider.
     * This way, we can launch a spider without dependency on the parser.
     * The basic use case of this is when you got your spider ready for a target,
     * but you don't have the parser yet : you'll just run it later.
     * If you have an incomplete version of your parser, or you wanna make an update,
     * same : just refresh the queue and re-parse all the data, with the spider still running on background.
     */
}
