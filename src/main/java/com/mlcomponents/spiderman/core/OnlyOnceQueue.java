
package com.mlcomponents.spiderman.core;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 * This a queue for which an element can't be added twice
 * @author antonin
 * @param <T> The type of the things we enqueue
 */
public class OnlyOnceQueue<T extends Dated> //The things must be dated (see refresh())
        implements Serializable {
    private final Queue<T> queue;
    private final HashSet<T> blacklist;

    /**
     * Make a queue
     */
    public OnlyOnceQueue() {
        this.queue = new LinkedList<>();
        this.blacklist = new HashSet<>();
    }
    
    /**
     * Enqueue something, if it's the first time
     * @param thing The thing
     */
    public void enqueue(T thing){
        boolean isNew = blacklist.add(thing);
        if(isNew){
            queue.add(thing);
        }
    }
    
    /**
     * Enqueue a list of things
     * @param things The list of things
     */
    public void enqueue(LinkedList<T> things){
        things.forEach((T thing) -> {
            enqueue(thing);
        });
    }
    
    /**
     * Dequeue something, if there is something.
     * @return Something if there is not nothing, or null.
     */
    public T dequeue(){
        return queue.poll();
    }
    
    /**
     * Reconstruct the queue with things that are older than dateLimit
     * or that doesn't have a date.
     * @param dateLimit 
     */
    public void refresh(Date dateLimit){
        queue.clear();
        blacklist.forEach((T thing) -> {
            if (thing.getDate() == null || thing.getDate().compareTo(dateLimit) < 0){
                queue.add(thing);
            }
        });
    }

}
