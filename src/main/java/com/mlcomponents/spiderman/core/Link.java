
package com.mlcomponents.spiderman.core;

import com.mlcomponents.spiderman.documents.AbstractDocument;
import com.mlcomponents.spiderman.parser.LinkStrategy;
import java.io.Serializable;
import java.net.URI;
import java.util.LinkedList;
import java.util.Objects;
import com.mlcomponents.spiderman.parser.LinkStrategyInterface;
import java.io.IOException;
import java.util.Date;

/**
 * This class contains useful informations about a link.
 * @author antonin
 * @param <ConcreteDocument> The type of document referred by the link
 */
public abstract class Link<ConcreteDocument extends AbstractDocument> //We can define the document type for a type of link
        implements Serializable, //We want to save those structures
                   LinkStrategyInterface<ConcreteDocument>, //Using a strategy make the handling of links very modular
                   Dated
{
    private final String referer;     /* Refering document */
    private final URI uri;         /* Actual URL of the link */
    private transient LinkStrategy<ConcreteDocument> linkStrategy;
    /**
     * Note that the strategy is made transient : we won't save it with the links.
     * That means that the strategy configuration must be static for a specific type of link.
     * The variable is not actually static, because then it would be a problem with inheritance.
     */
    
    private ConcreteDocument document; /* The associated document */
    
    /**
     *
     * @param referer
     * @param uri
     * @param linkStrategy
     * @param document
     */
    public Link(String referer, URI uri, LinkStrategy linkStrategy, ConcreteDocument document) {
        this.referer = referer;
        this.uri = uri;
        this.linkStrategy = linkStrategy;
        this.document = document;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o){
        if(o instanceof Link){
            return ((Link) o).uri.equals(this.uri);
        }
        else{
            return false;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.uri);
        return hash;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString(){
        return "["
                + "uri=" + uri + "; "
                + "referer=" + referer + "; "
                + "document=" + document + "; "
                + "]";
    }
    
    @Override
    public LinkedList extractLinks(ConcreteDocument document) {
        return linkStrategy.extractLinks(document);
    }
    
    @Override
    public String subPathFromURL(URI url){
        return linkStrategy.subPathFromURL(url);
    }

    @Override
    public void operationBefore(ConcreteDocument document) {
        linkStrategy.operationBefore(document);
    }

    @Override
    public void extractData(ConcreteDocument document) {
        linkStrategy.extractData(document);
    }

    @Override
    public void operationAfter(ConcreteDocument document) {
        linkStrategy.operationAfter(document);
    }
    
    /**
     *
     * @return
     */
    public URI getURI(){
        return uri;
    }
    
    /**
     *
     * @return
     */
    public AbstractDocument getDocument(){
        return this.document;
    }
    
    /**
     *
     * @param document
     */
    public void setDocument(ConcreteDocument document){
        this.document = document;
    }

    @Override
    public Date getDate() {
        return document.getDownloadDate();
    }

    @Override
    public void setDate(Date date) {
        this.document.setDownloadDate(date);
    }
    
    /**
     * A hook to restore the strategy when the link is de-serialized.
     * @param in
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    private void readObject(java.io.ObjectInputStream in)
     throws IOException, ClassNotFoundException{
        in.defaultReadObject();
        setStrategy(getDefaultStrategy());
    }
    
    /**
     *
     * @param strategy
     */
    public void setStrategy(LinkStrategy<ConcreteDocument> strategy){
        this.linkStrategy = strategy;
    }
    
    /**
     * As said before, the design doesn't clearly reflect this but,
     * the configuration of the strategy of links is meant to be static.
     * We force all the concrete classes of links to provide a function to give
     * their strategy.
     * @return
     */
    public abstract LinkStrategy<ConcreteDocument> getDefaultStrategy();
    
}
