/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlcomponents.spiderman.core;

/**
 *
 * @author antonin
 */
public class SpiderInterface {
    /**
     * Here you can do an awesome textual interface. Have fun !
     * Hint : use observers, and mediators to avoid tight coupling.
     * 
     * More explanations :
     * When we get the scrapers (spider and parser) to run in parallel, with multiple
     * targets, it would be could to have an output on the screen that is readable.
     * This is why a class would be useful just to get some global informations about
     * the scrapers that are running and show them in real time.
     * It could be an interface with fixed informations (in the style of the "top" program).
     * 
     * This class is here as a remainder, but we'll need multiple classes to make an interface.
     * A package com.mlcomponents.spiderman.interface would be needed. Then, a class to actually run all the scrapers
     * (so not in the "main" function anymore), used as a mediator for the threads to communicate with the interface.
     * This has to be done with mediators and observers, to avoid tight coupling. The idea is that a thread could be observable,
     * but not aware of its observers, nor the existence of an interface. Yes, that's MVC.
     */
}
