
package com.mlcomponents.spiderman.core;

import com.mlcomponents.spiderman.documents.AbstractDocument;
import java.io.Serializable;
import java.util.Date;

/**
 * The context of the spider.
 * This class contains the data about a scraping session : all the extracted links,
 * and the documents.
 * TO DO : make it thread safe, and make the useful tools for threads communication/synchronisation about this.
 * @author antonin
 */
public class SpiderContext implements Serializable {
    private final OnlyOnceQueue<Link> linksQueue;
    private final OnlyOnceQueue<AbstractDocument> docsQueue;

    /**
     *
     */
    public SpiderContext() {
        this.linksQueue = new OnlyOnceQueue<>();
        this.docsQueue = new OnlyOnceQueue<>();
    }
    
    /**
     *
     * @param dateLimit
     */
    public void refresh(Date dateLimit){
        linksQueue.refresh(dateLimit);
        docsQueue.refresh(dateLimit);
    }
    
    //TODO : Refresh on deserialization

    /**
     *
     * @return
     */
    
    public OnlyOnceQueue<Link> getLinks(){
        return linksQueue;
    }
    
    /**
     *
     * @return
     */
    public OnlyOnceQueue<AbstractDocument> getDocuments(){
        return docsQueue;
    }
    
}
