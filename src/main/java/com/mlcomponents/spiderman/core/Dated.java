/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlcomponents.spiderman.core;

import java.util.Date;

/**
 * An interface to indicate that a class has a date.
 * @author antonin
 */
public interface Dated {

    /**
     *
     * @return
     */
    public Date getDate();

    /**
     *
     * @param date
     */
    public void setDate(Date date);
}
