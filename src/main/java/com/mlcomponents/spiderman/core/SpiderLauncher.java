/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlcomponents.spiderman.core;

import com.mlcomponents.spiderman.network.DownloadManager;
import com.mlcomponents.spiderman.targets.Target;
import com.mlcomponents.spiderman.tools.SerializationHelper;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author antonin
 */
public class SpiderLauncher {
    private final Target target;
    private final DownloadManager downloadManager;
    private SpiderContext context;
    
    private Thread spiderThread = null;
    private final ScheduledExecutorService scheduler;
    
    private final static int AUTO_SAVE_DELAY = 5;
    private final static TimeUnit AUTO_SAVE_DELAY_UNIT = TimeUnit.MINUTES;
    
    /**
     * The class that launch a spider
     * TODO : something to launch a parser, then something to launch both
     * @param target
     * @param downloadManager
     * @throws ClassNotFoundException
     */
    public SpiderLauncher(Target target, DownloadManager downloadManager) throws ClassNotFoundException {
        this.target = target;
        this.downloadManager = downloadManager;
        //Try to resume an instance of the spider
        try {
            this.context = (SpiderContext) SerializationHelper.deSerialization(target.getContextPath());
        } catch (IOException ex) {
            this.context = new SpiderContext();
            Logger.getLogger(SpiderLauncher.class.getName()).log(Level.WARNING, "No saved data found. Creating new instance.", ex);
        }
        System.out.println(context);
        
        //Save the state of the scraper periodically, using serialization
        this.scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new SaveStateTask(), AUTO_SAVE_DELAY, AUTO_SAVE_DELAY, AUTO_SAVE_DELAY_UNIT);
        
        //TODO : Run another thread for the parser
    }
    
    /**
     *
     */
    public void start(){
        if(spiderThread != null) return;
        spiderThread = new Thread(new Spider(downloadManager, target, context.getLinks()));
        spiderThread.setName(target.getContextPath());
        spiderThread.start();
    }
    
    /**
     *
     * @throws InterruptedException
     */
    public void join() throws InterruptedException{
        if(spiderThread == null) return;
        spiderThread.join();
    }
    
    private void save() throws IOException{
        SerializationHelper.serialization(target.getContextPath(), context);
    }
    
    private class SaveStateTask implements Runnable {
        
        @Override
        public void run() {
            try {
                SpiderLauncher.this.save();
            } catch (IOException ex) {
                Logger.getLogger(SpiderLauncher.class.getName()).log(Level.SEVERE, "Unable to save the context", ex);
            }
        }
    
    }
    
}
