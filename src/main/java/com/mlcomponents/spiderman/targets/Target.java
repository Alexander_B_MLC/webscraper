
package com.mlcomponents.spiderman.targets;

import com.mlcomponents.spiderman.core.Link;

/**
 *
 * @author antonin
 */
public abstract class Target {
    
    /**
     *
     */
    public static final String GLOBAL_PATH_BASE = "./out/";
    private final Link startLink;

    /**
     *
     * @param startLink
     */
    public Target(Link startLink) {
        this.startLink = startLink;
    }
    
    /**
     *
     * @return
     */
    public Link getStartLink(){
        return startLink;
    }
    
    /**
     *
     * @return
     */
    public String getName(){
        return this.getClass().getSimpleName();
    }
    
    /**
     *
     * @return
     */
    public String getContextPath(){
        return Target.GLOBAL_PATH_BASE + getName() + ".ser";
    }
    
}
