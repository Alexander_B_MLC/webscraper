
package com.mlcomponents.spiderman.targets.digikey;

import com.mlcomponents.spiderman.targets.Target;
import java.net.URISyntaxException;

/**
 *
 * @author antonin
 */
public class Digikey extends Target {
    
    /**
     *
     */
    public final static int PRODUCTS_PER_PAGE = 500;

    /**
     *
     */
    public final static String LOCAL_PATH = GLOBAL_PATH_BASE+"Digikey";

    /**
     *
     */
    public final static String DIGIKEY_HOME = "https://www.mlcomponents.com";
    
    /**
     *
     */
    public final static DigikeyLinkAllProductsBuilder DIGIKEY_LINK_ALL_PRODUCTS_BUILDER = new DigikeyLinkAllProductsBuilder();

    /**
     *
     */
    public final static DigikeyLinkProductsBuilder DIGIKEY_LINK_PRODUCTS_BUILDER = new DigikeyLinkProductsBuilder();
    
    /**
     *
     * @throws URISyntaxException
     */
    public Digikey() throws URISyntaxException{
        super(DIGIKEY_LINK_ALL_PRODUCTS_BUILDER.build());
    }

}
