
package com.mlcomponents.spiderman.targets.digikey;

import com.mlcomponents.spiderman.documents.HtmlDocument;
import com.mlcomponents.spiderman.parser.LinkStrategy;
import com.mlcomponents.spiderman.parser.LinkStrategyInterface;
import com.mlcomponents.spiderman.parser.NoDataExtractionStrategy;
import com.mlcomponents.spiderman.parser.NoLinkExtractionStrategy;
import com.mlcomponents.spiderman.core.Link;
import java.net.URI;

/**
 *
 * @author antonin
 */
public class DigikeyLinkProducts extends DigikeyLink {
    
    private static final LinkStrategy DEFAULT_STRATEGY =
            new LinkStrategy(
                    new NoLinkExtractionStrategy(),
                    new NoDataExtractionStrategy() // TEMPORARY
            );
    
    //private final numberOfProducts;

    /**
     *
     * @param referer
     * @param uri
     * @param localPath
     * @param customStrategy
     */
    
    public DigikeyLinkProducts(String referer, URI uri, String localPath, LinkStrategy customStrategy){
        super(referer, uri, customStrategy, new HtmlDocument(localPath));
    }
    
    /**
     *
     * @param referer
     * @param uri
     * @param localPath
     */
    public DigikeyLinkProducts(String referer, URI uri, String localPath){
        this(referer, uri, localPath, DEFAULT_STRATEGY);
    }

    /**
     *
     * @return
     */
    @Override
    public LinkStrategy<HtmlDocument> getDefaultStrategy(){
        return DEFAULT_STRATEGY;
    }
    
}
