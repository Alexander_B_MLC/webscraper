/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlcomponents.spiderman.targets.digikey;

import com.mlcomponents.spiderman.documents.HtmlDocument;
import com.mlcomponents.spiderman.parser.LinkExtractorStrategy;
import com.mlcomponents.spiderman.core.Link;
import com.mlcomponents.spiderman.tools.SingleGroupMatcher;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author antonin
 */
public class DigikeyLinkExtractorAllProducts implements LinkExtractorStrategy<HtmlDocument> {

    private final DigikeyLinkProductsBuilder linkBuilder;
    
    private final SingleGroupMatcher numberMatcher = new SingleGroupMatcher("\\(([0-9]*) items\\)");
    private final SingleGroupMatcher categoryMatcher = new SingleGroupMatcher("(?:\\/products\\/en)([^0-9]+)");
    //private final SingleGroupMatcher decimalFVPattern = new SingleGroupMatcher("\\/([0-9]*)$");

    /**
     *
     * @param linkBuilder
     */
    public DigikeyLinkExtractorAllProducts(DigikeyLinkProductsBuilder linkBuilder) {
        this.linkBuilder = linkBuilder;
        linkBuilder.setPageSize(Digikey.PRODUCTS_PER_PAGE);//If you want to change this, change also in extractLink
    }

    /**
     *
     */
    public DigikeyLinkExtractorAllProducts() {
        this(Digikey.DIGIKEY_LINK_PRODUCTS_BUILDER);
    }
       
    /**
     *
     * @param document
     * @return
     */
    @Override
    public LinkedList<Link> extractLinks(HtmlDocument document) {
        LinkedList<Link> links = new LinkedList<>();
        Document parsedHTML = document.getParsedDocument();

        Elements productsItems = parsedHTML.select("ul.catfiltersub").select("li");
        productsItems.forEach((Element item) -> {
            String path = item.select("a.catfilterlink").attr("href");
            String text = item.text();

            //int nuberOfItems = Integer.valueOf(numberMatcher.getSingleGroup(text));
            int numberOfPages = 2;
            
            for(int i = 1; i < numberOfPages; i++){
                try {
                    linkBuilder.setPath(new URI(path).getPath());
                } catch (URISyntaxException ex) {
                    Logger.getLogger(DigikeyLinkExtractorAllProducts.class.getName()).log(Level.SEVERE, null, ex);
                }
                linkBuilder.setPageNumber(i);
                try {
                    links.add(linkBuilder.build());
                } catch (URISyntaxException ex) {
                    Logger.getLogger(DigikeyLinkExtractorAllProducts.class.getName()).log(Level.SEVERE, null, ex);
                }
           }
        });
        
        return links;
    }

    /**
     *
     * @param url
     * @return
     */
    @Override
    public String subPathFromURL(URI url) {
        return url.getPath();
    }

}
