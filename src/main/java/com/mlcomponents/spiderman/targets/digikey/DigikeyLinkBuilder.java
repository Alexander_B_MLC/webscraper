
package com.mlcomponents.spiderman.targets.digikey;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;
import org.apache.http.client.utils.URIBuilder;

/**
 *
 * @author antonin
 * @param <DigikeyLinkType>
 */
public abstract class DigikeyLinkBuilder<DigikeyLinkType extends DigikeyLink> {
    private static final Logger LOGGER = Logger.getLogger(DigikeyLinkBuilder.class.getName());
    
    private final URIBuilder uriBuilder;
    private String referer;
    
    /**
     *
     */
    public DigikeyLinkBuilder() {
        uriBuilder = new URIBuilder();
        
        //Default configuration
        uriBuilder.setScheme("https");
        uriBuilder.setHost("www.digikey.com");
        uriBuilder.setPath("products/en");
        this.referer = "https://www.digikey.com";
    }
    
    /**
     *
     * @param name
     * @param set
     * @return
     */
    protected DigikeyLinkBuilder setFilter(String name, boolean set){
        uriBuilder.setParameter(name, set?"1":"0");
        return this;
    }
    
    /**
     *
     * @param set
     * @return
     */
    public DigikeyLinkBuilder setInStockFilter(boolean set){
        return setFilter("stock", set);
    }
    
    /**
     *
     * @param set
     * @return
     */
    public DigikeyLinkBuilder setNormallyStockingFilter(boolean set){
        return setFilter("nstock", set);
    }
    
    /**
     *
     * @param set
     * @return
     */
    public DigikeyLinkBuilder setNewProductsFilter(boolean set){
        return setFilter("newproducts", set);
    }
    
    /**
     *
     * @param set
     * @return
     */
    public DigikeyLinkBuilder setDatasheetFilter(boolean set){
        return setFilter("datasheet", set);
    }
    
    /**
     *
     * @param set
     * @return
     */
    public DigikeyLinkBuilder setPhotoFilter(boolean set){
        return setFilter("photo", set);
    }
    
    /**
     *
     * @param set
     * @return
     */
    public DigikeyLinkBuilder setCADModelFilter(boolean set){
        return setFilter("cad", set);
    }
    
    /**
     *
     * @param set
     * @return
     */
    public DigikeyLinkBuilder setRoHSCompliantFilter(boolean set){
        setFilter("nonrohs", !set);
        return setFilter("rohs", set);
    }
    
    private DigikeyLinkBuilder setNonRoHSCompliantFilter(boolean set){//seems useless
        return setRoHSCompliantFilter(!set);
    }
    
    /**
     *
     * @param keyword
     * @return
     */
    public DigikeyLinkBuilder setKeywordFilter(String keyword){
        uriBuilder.setParameter("keywords", keyword);
        return this;
    }
    
    /**
     *
     * @param referer
     * @return
     */
    public DigikeyLinkBuilder setReferer(String referer){
        this.referer = referer;
        return this;
    }
    
    /**
     *
     * @return
     * @throws URISyntaxException
     */
    public URI getCurrentURI() throws URISyntaxException{
        return uriBuilder.build();
    }
    
    /**
     *
     * @return
     */
    public String getReferer(){
        return referer;
    }
    
    /**
     *
     * @return
     */
    protected final URIBuilder getURIBuilder(){
        return uriBuilder;
    }
    
    /**
     *
     * @return
     * @throws URISyntaxException
     */
    public abstract DigikeyLinkType build() throws URISyntaxException;
}
