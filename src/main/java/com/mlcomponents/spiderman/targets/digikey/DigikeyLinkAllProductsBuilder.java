
package com.mlcomponents.spiderman.targets.digikey;

import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author antonin
 */
public class DigikeyLinkAllProductsBuilder extends DigikeyLinkBuilder<DigikeyLinkAllProducts> {

    /**
     *
     * @return
     * @throws URISyntaxException
     */
    @Override
    public DigikeyLinkAllProducts build() throws URISyntaxException{
        URI uri = getURIBuilder().build();
        
        return new DigikeyLinkAllProducts(
                getReferer(),
                uri,
                Digikey.LOCAL_PATH + uri.getPath() + ".html"
        );
    }
}
