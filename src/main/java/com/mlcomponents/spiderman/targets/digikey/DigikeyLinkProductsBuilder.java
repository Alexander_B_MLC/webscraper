
package com.mlcomponents.spiderman.targets.digikey;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

/**
 *
 * @author antonin
 */
public class DigikeyLinkProductsBuilder extends DigikeyLinkBuilder<DigikeyLinkProducts> {
    private static final Logger LOGGER = Logger.getLogger(DigikeyLinkProductsBuilder.class.getName());
    
    private String path;
    private String subLocalPath;
    
    private int page = 1;
    
    /**
     *
     */
    public DigikeyLinkProductsBuilder() {
        super();
        this.path = "";
        this.subLocalPath = "";
    }
    
    /**
     *
     * @param path
     */
    public DigikeyLinkProductsBuilder(String path) {
        this();
        getURIBuilder().setPath(path);
    }
    
    /**
     *
     * @param keyword
     * @return
     */
    @Override
    public DigikeyLinkProductsBuilder setKeywordFilter(String keyword){
        getURIBuilder().setParameter("k", keyword);
        return this;
    }
    
    /**
     *
     * @param pageSize
     * @return
     */
    public DigikeyLinkProductsBuilder setPageSize(int pageSize){
        getURIBuilder().setParameter("pageSize", String.valueOf(pageSize));
        return this;
    }
    
    /**
     *
     * @param page
     * @return
     */
    public DigikeyLinkProductsBuilder setPageNumber(int page){
        if(page < 1){
            LOGGER.warning("Invalid page number, falling back to 1");
            return this;
        }
        this.page = page;
        return this;
    }
    
    /**
     *
     * @param path
     * @return
     */
    public DigikeyLinkProductsBuilder setPath(String path){
        this.path = path;
        return this;
    }
    
    /**
     *
     * @return
     * @throws URISyntaxException
     */
    @Override
    public DigikeyLinkProducts build() throws URISyntaxException{
        getURIBuilder().setPath(path + "/page/" + String.valueOf(this.page));
        URI uri = getURIBuilder().build();
        System.out.println(" LINK BULDER " + uri.toString());
        
        return new DigikeyLinkProducts(
                getReferer(),
                uri,
                Digikey.LOCAL_PATH + uri.getPath() + ".html"
        );
    }
}
