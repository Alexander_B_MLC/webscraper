/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlcomponents.spiderman.targets.digikey;

import com.mlcomponents.spiderman.core.Link;
import com.mlcomponents.spiderman.documents.HtmlDocument;
import com.mlcomponents.spiderman.parser.LinkStrategy;
import java.net.URI;

/**
 *
 * @author antonin
 */
public abstract class DigikeyLink extends Link<HtmlDocument> {
    
    /**
     *
     * @param referer
     * @param uri
     * @param linkStrategy
     * @param document
     */
    public DigikeyLink(String referer, URI uri, LinkStrategy linkStrategy, HtmlDocument document) {
        super(referer, uri, linkStrategy, document);
    }
    
}
