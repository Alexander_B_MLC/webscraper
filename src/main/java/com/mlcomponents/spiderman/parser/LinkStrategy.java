/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlcomponents.spiderman.parser;

import com.mlcomponents.spiderman.documents.AbstractDocument;
import java.net.URI;
import java.util.LinkedList;

/**
 *
 * @author antonin
 * @param <A>
 */
public class LinkStrategy<A extends AbstractDocument> implements LinkStrategyInterface<A>{

    private final LinkExtractorStrategy linkExtractorStrategy;
    private final DataExtractorStrategy dataExtractorStrategy;

    /**
     *
     * @param linkExtractorStrategy
     * @param dataExtractorStrategy
     */
    public LinkStrategy(LinkExtractorStrategy linkExtractorStrategy, DataExtractorStrategy dataExtractorStrategy) {
        this.linkExtractorStrategy = linkExtractorStrategy;
        this.dataExtractorStrategy = dataExtractorStrategy;
    }
    
    /**
     *
     * @param document
     * @return
     */
    @Override
    public LinkedList extractLinks(A document) {
        return linkExtractorStrategy.extractLinks(document);
    }
    
    /**
     *
     * @param url
     * @return
     */
    @Override
    public String subPathFromURL(URI url) {
        return this.linkExtractorStrategy.subPathFromURL(url);
    }

    /**
     *
     * @param document
     */
    @Override
    public void operationBefore(A document) {
        dataExtractorStrategy.operationBefore(document);
    }

    /**
     *
     * @param document
     */
    @Override
    public void extractData(A document) {
        dataExtractorStrategy.extractData(document);
    }

    /**
     *
     * @param document
     */
    @Override
    public void operationAfter(A document) {
        dataExtractorStrategy.operationAfter(document);
    }
    
}
