
package com.mlcomponents.spiderman.parser;

import com.mlcomponents.spiderman.documents.AbstractDocument;
import com.mlcomponents.spiderman.core.Link;
import java.net.URI;
import java.util.LinkedList;

/**
 *
 * @author antonin
 * @param <A>
 */
public interface LinkExtractorStrategy<A extends AbstractDocument>{

    /**
     *
     * @param document
     * @return
     */
    public LinkedList<Link> extractLinks(A document);

    /**
     *
     * @param url
     * @return
     */
    public String subPathFromURL(URI url);
    /**
     * Defining a proper subPathFromURL could seem like an overkill because it's easy to do.
     * However, we have to be very careful when doing this operation : we may
     * break our local copy of the web site by having collisions with the
     * association URL --> localPath.
     * When implementing this function, one should ensure that two different web
     * pages NEVER have the same sub local path.
     */
}
