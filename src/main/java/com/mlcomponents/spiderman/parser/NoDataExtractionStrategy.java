
package com.mlcomponents.spiderman.parser;

import com.mlcomponents.spiderman.documents.AbstractDocument;

/**
 *
 * @author antonin
 */
public class NoDataExtractionStrategy implements DataExtractorStrategy {

    /**
     *
     * @param document
     */
    @Override
    public void operationBefore(AbstractDocument document) {}

    /**
     *
     * @param document
     */
    @Override
    public void extractData(AbstractDocument document) {}

    /**
     *
     * @param document
     */
    @Override
    public void operationAfter(AbstractDocument document) {}
    
}
