
package com.mlcomponents.spiderman.parser;

import com.mlcomponents.spiderman.documents.AbstractDocument;

/**
 *
 * @author antonin
 * @param <A>
 */
public interface DataExtractorStrategy<A extends AbstractDocument> {

    /**
     *
     * @param document
     */
    public void operationBefore(A document);

    /**
     *
     * @param document
     */
    public void extractData(A document);

    /**
     *
     * @param document
     */
    public void operationAfter(A document);
}
