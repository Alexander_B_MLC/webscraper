
package com.mlcomponents.spiderman.parser;

import com.mlcomponents.spiderman.documents.AbstractDocument;

/**
 *
 * @author antonin
 * @param <A>
 */
public interface LinkStrategyInterface<A extends AbstractDocument> extends LinkExtractorStrategy<A>,DataExtractorStrategy<A>{}
