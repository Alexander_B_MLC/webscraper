
package com.mlcomponents.spiderman.parser;

import com.mlcomponents.spiderman.documents.AbstractDocument;
import com.mlcomponents.spiderman.core.Link;
import java.net.URI;
import java.util.LinkedList;

/**
 *
 * @author antonin
 */
public class NoLinkExtractionStrategy implements LinkExtractorStrategy<AbstractDocument> {

    /**
     *
     * @param document
     * @return
     */
    @Override
    public LinkedList<Link> extractLinks(AbstractDocument document) {
        return new LinkedList<>();
    }

    /**
     *
     * @param url
     * @return
     */
    @Override
    public String subPathFromURL(URI url) {
        return "";
    }
    
}
