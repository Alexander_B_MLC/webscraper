
package com.mlcomponents.spiderman.database;
/**
 * This package is going to define all the classes necessary to represent components,
 * and save them (in files, or in a database for example)
 */

/**
 * A class to represent a component.
 * This should implement all the necessary attributes/methods to represent a component,
 * with some fields that will be common to all components and some that will be optional
 * (maybe implemented in sub-classes).
 * @author antonin
 */
public class BasicComponent {

    /**
     * 
     */
    public BasicComponent() {
        
    }
    
}
