
package com.mlcomponents.spiderman.documents;

/**
 *
 * @author antonin
 */
public class ImageDocument extends AbstractBinaryDocument {
    
    /**
     *
     * @param localPath
     * @param data
     */
    public ImageDocument(String localPath, byte[] data) {
        super(localPath, data);
    }
    
}
