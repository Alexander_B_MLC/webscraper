
package com.mlcomponents.spiderman.documents;

/**
 *
 * @author antonin
 */
public class PDFDocument extends AbstractBinaryDocument {
    
    /**
     *
     * @param localPath
     * @param data
     */
    public PDFDocument(String localPath, byte[] data) {
        super(localPath, data);
    }
    
}
