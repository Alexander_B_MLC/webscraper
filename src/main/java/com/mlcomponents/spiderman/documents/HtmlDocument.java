
package com.mlcomponents.spiderman.documents;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author antonin
 */
public class HtmlDocument extends AbstractTextualDocument {
    
    private transient Document parsedDocument;
    
    /**
     *
     * @param localPath
     * @param text
     */
    public HtmlDocument(String localPath, String text) {
        super(localPath, text);
        this.parsedDocument = null;
    }

    /**
     *
     * @param localPath
     */
    public HtmlDocument(String localPath) {
        this(localPath, null);
    }
    
    /**
     *
     * @return
     */
    public Document parse(){
        return Jsoup.parse(getData());
    }
    
    /**
     *
     * @return
     */
    public Document getParsedDocument(){
        String data = getData();
        if(data == null) return null;
        if(parsedDocument == null) parsedDocument = parse();
        return parsedDocument;
    }
    
    
}
