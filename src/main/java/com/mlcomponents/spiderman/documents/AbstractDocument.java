
package com.mlcomponents.spiderman.documents;

import com.mlcomponents.spiderman.core.Dated;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * The documents are made to facilitate I/O and basic processing of data.
 * @author antonin
 * @param <RawData>
 */
public abstract class AbstractDocument <RawData> implements Serializable,Dated {
    
    private Date parseDate;     /* parse date, null if data wasn't extracted yet */
    private Date downloadDate;  /* dowload date, null if not yet downloaded */
    private String localPath;   /* Path of the file */
    private transient RawData data; /* The actual data if downloaded/loaded, null otherwise */

    /**
     *
     * @param localPath
     * @param data
     */
    public AbstractDocument(String localPath, RawData data){
        if (localPath == null) throw new IllegalArgumentException("localPath must be a valid string");
        this.localPath = localPath;
        this.data = data;
        this.parseDate = null;
    }
    
    /**
     *
     * @param localPath
     */
    public AbstractDocument(String localPath) {
        this(localPath, null);
    }
    
    /**
     *
     * @throws IOException
     */
    public abstract void saveToFile() throws IOException;

    /**
     *
     * @throws IOException
     */
    public abstract void loadFromFile() throws IOException;
    
    /**
     *
     * @param data
     */
    public final void setData(RawData data){
        this.data = data;
    }
    
    /**
     *
     * @return
     */
    public RawData getData(){
        //TODO : Auto load from file if the file exists, throw a IOException otherwise
        return data;
    }
    
    /**
     *
     * @return
     */
    public String getLocalPath(){
        return localPath;
    }
    
    /**
     *
     * @param date
     */
    public void setParseDate(Date date){
        this.parseDate = date;
    }
    
    /**
     *
     * @return
     */
    public Date getParseDate(){
        return this.parseDate;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof AbstractDocument)
            return (((AbstractDocument) o).localPath.equals(this.localPath));
        else
            return false;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.localPath);
        return hash;
    }

    /**
     *
     * @return
     */
    @Override
    public Date getDate() {
        return this.parseDate;
    }

    /**
     *
     * @param date
     */
    @Override
    public void setDate(Date date) {
        this.parseDate = date;
    }
    
    /**
     *
     * @return
     */
    public Date getDownloadDate() {
        return this.downloadDate;
    }

    /**
     *
     * @param date
     */
    public void setDownloadDate(Date date) {
        this.parseDate = date;
    }
}
