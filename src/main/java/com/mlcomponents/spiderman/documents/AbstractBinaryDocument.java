
package com.mlcomponents.spiderman.documents;

import java.io.IOException;

/**
 * 
 * @author antonin
 */
public abstract class AbstractBinaryDocument extends AbstractDocument<byte[]>{

    /**
     *
     * @param localPath
     * @param data
     */
    public AbstractBinaryDocument(String localPath, byte[] data) {
        super(localPath, data);
    }

    /**
     *
     * @throws IOException
     */
    @Override
    public void saveToFile() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @throws IOException
     */
    @Override
    public void loadFromFile() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
