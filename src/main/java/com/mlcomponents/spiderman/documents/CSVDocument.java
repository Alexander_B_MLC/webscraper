
package com.mlcomponents.spiderman.documents;

/**
 *
 * @author antonin
 */
public class CSVDocument extends AbstractTextualDocument {
    
    /**
     *
     * @param localPath
     * @param text
     */
    public CSVDocument(String localPath, String text) {
        super(localPath, text);
    }
    
}
