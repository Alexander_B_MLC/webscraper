package com.mlcomponents.spiderman.documents;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author antonin
 */
public abstract class AbstractTextualDocument extends AbstractDocument<String> {

    /**
     *
     * @param localPath
     * @param text
     */
    public AbstractTextualDocument(String localPath, String text) {
        super(localPath, text);
    }

    /**
     *
     * @param localPath
     */
    public AbstractTextualDocument(String localPath) {
        this(localPath, null);
    }

    /**
     *
     * @throws IOException
     */
    @Override
    public void saveToFile() throws IOException {
        String path = getLocalPath();
        File file = new File(path);
        file.getParentFile().mkdirs();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            writer.write(getData());
        }
    }

    /**
     *
     * @throws IOException
     */
    @Override
    public void loadFromFile() throws IOException {
        String path = getLocalPath();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            setData(builder.toString());
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return getData();
    }
}
