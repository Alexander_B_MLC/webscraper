package com.mlcomponents.spiderman.network;


import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;

import org.apache.http.*;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLException;

/**
 * This class is meant to manage HTTP requests.
 *
 * @author antonin
 */
public class HttpHandler implements HttpGetInterface {

    private final SSLConnectionSocketFactory sslCSF;
    private final CloseableHttpClient client;

    //Default headers
    private static final String DEFAULT_USERAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0";
    private static final String DEFAULT_ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
    private static final String DEFAULT_ACCEPT_LANGUAGE = "en-GB,en;q=0.5";
    private static final int TIMEOUT = 5000;

    private enum HttpHandlerFormat {
        TEXT, RAW
    }

    /**
     *
     * @throws KeyStoreException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    public HttpHandler() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        /**
         * Making an SSLConnectionSocketFactory. It allows us to make SSL
         * sockets and trust every server. We don't care about security here,
         * but then we can make HTTPS requests.
         */
        SSLContextBuilder contextBuilder = new SSLContextBuilder();
        contextBuilder.loadTrustMaterial(new TrustAllStrategy());
        this.sslCSF = new SSLConnectionSocketFactory(contextBuilder.build());

        //Setting default headers
        LinkedList<Header> defaultHeaders = new LinkedList<>();
        defaultHeaders.add(new BasicHeader(HttpHeaders.ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
        defaultHeaders.add(new BasicHeader(HttpHeaders.ACCEPT_LANGUAGE, "en-GB,en;q=0.5"));
        defaultHeaders.add(new BasicHeader(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate, br"));
        defaultHeaders.add(new BasicHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0"));

        //HttpClient
        this.client = HttpClients.custom()
        	.setRetryHandler(retryHandler())
                .setSSLSocketFactory(this.sslCSF)
                .setDefaultHeaders(defaultHeaders)
                .build();
    }

    private CloseableHttpResponse executeRequest(URI uri, InetAddress bindAddress) throws IOException {
        //Create a request for the given uri
        HttpGet request = new HttpGet(uri);

        //Configure the request to bind to a specific address
        RequestConfig requestConfig = RequestConfig.custom()
                .setLocalAddress(bindAddress)
                .setConnectTimeout(TIMEOUT)
                .setSocketTimeout(TIMEOUT)
                .build();
        request.setConfig(requestConfig);
     	   return client.execute(request);
    }

	private static HttpRequestRetryHandler retryHandler(){
		return ( exception, executionCount, context ) -> {
		System.out.println("try request : " + executionCount);

		if(executionCount >=5) {
		return false;
		}

		if(exception instanceof InterruptedIOException) {
		return false;
		}

		if(exception instanceof UnknownHostException) {
		return false;
		}

		if(exception instanceof SSLException) {
		return false;
		}

		HttpClientContext clientContext = HttpClientContext.adapt(context);
		HttpRequest request = clientContext.getRequest();
		boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
		if (idempotent){
			return true;
			}
		return false;
		};
	}





    /**
     *
     * @param uri
     * @param localAddress
     * @return
     * @throws IOException
     */
    @Override
    public String getText(URI uri, InetAddress localAddress) throws IOException {
        CloseableHttpResponse response = executeRequest(uri, localAddress);
        HttpEntity entity;
        String text = null;
        try {
            entity = response.getEntity();
            if (entity != null) {
                text = EntityUtils.toString(entity);
                EntityUtils.consume(entity);
            }
        } finally {
            response.close();
        }
        return text;
    }

    /**
     * TODO : Check that the request execute correctly (HTTP 200)
     * Check if the loaded page is not a page for an error (specific to targets, not here)
     */
    
    /**
     *
     * @param uri
     * @param localAddress
     * @return
     * @throws IOException
     */
    @Override
    public byte[] getByteArray(URI uri, InetAddress localAddress) throws IOException {
        CloseableHttpResponse response = executeRequest(uri, localAddress);
        HttpEntity entity;
        byte[] array = null;
        try {
            entity = response.getEntity();
            if (entity != null) {
                array = EntityUtils.toByteArray(entity);
            }
        } finally {
            response.close();
        }
        return array;
    }
}
