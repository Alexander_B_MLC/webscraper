
package com.mlcomponents.spiderman.network;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class to manage an OpenVPN client.
 *
 * @author antonin
 */
public class VPN extends NIC {

    private static final Logger LOGGER = Logger.getLogger(VPN.class.getName());

    //Timeout for VPN initialization
    private final long VPN_TIMEOUT = 10000;

    private String name;
    private String configPath; //path to the .ovpn file
    private Process openVPNProcess;

    /**
     * Main constructor.
     *
     * @param name Name of the VPN client to create
     * @param configPath Configuration file for this VPN
     */
    public VPN(String name, String configPath) {
        super();
        //Checking the arguments
        if (name == null || configPath == null) {
            throw new IllegalArgumentException("Arguments should'nt be null.");
        }

        this.name = name;
        this.configPath = configPath;
        this.openVPNProcess = null;
    }

    /**
     * Connection to the VPN
     * @return True if it succeeds, false otherwise.
     */
    public boolean connect() {
        do {
            //Checking configuration and gathering Network Interface name
            String dev = checkNicConfiguration();
            if (dev == null) {
                Logger.getLogger(VPN.class.getName()).log(Level.SEVERE, "Could not start OpenVPN : invalid network interface");
                break;
            }

            //Closing OpenVPN if it is already open
            disconnect();

            //Starting OpenVPN            
            try {

                ProcessBuilder pb = new ProcessBuilder("openvpn", new File(configPath).getName());
                pb.directory(new File(configPath).getParentFile()); //We start it in the config folder
                openVPNProcess = pb.start(); //Run

            } catch (IOException ex) {
                Logger.getLogger(VPN.class.getName()).log(Level.SEVERE, "Could not start OpenVPN : IO error", ex);
                break;
            }

            //Waiting for the connexion to be ready
            long start = System.currentTimeMillis();

            //First, we try to get the new interface
            NetworkInterface tun = null;
            do {
                try {
                    tun = NetworkInterface.getByName(dev);
                    if (tun != null && tun.isUp()) {
                        break;
                    } else {
                        Thread.sleep(500);
                    }
                    /* It would be good to have a smarter way to know when the
                    interface is ready without sleeping (for example reading the
                    OpenVPN logs). But that's not a priority. */
                } catch (SocketException ex) {
                    Logger.getLogger(VPN.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(VPN.class.getName()).log(Level.WARNING, null, ex);
                }
            } while ((System.currentTimeMillis() - start) < VPN_TIMEOUT);
            if (tun == null) {
                break;
            }
            setNetworkInterface(tun);
            LOGGER.log(Level.INFO, "Interface {0} is now UP", tun);

            do {
                if (checkConnectivity()) {
                    break;
                }
                try {
                    Thread.sleep(500); //Same remark here
                } catch (InterruptedException ex) {
                    Logger.getLogger(VPN.class.getName()).log(Level.SEVERE, null, ex);
                }
            } while ((System.currentTimeMillis() - start) < VPN_TIMEOUT);
            if ((System.currentTimeMillis() - start) >= VPN_TIMEOUT) {
                break;
            }
            LOGGER.log(Level.INFO, "Interface {0} successfully connected.", tun);

            return true;
        } while (false);

        /*
        * If the do..while loop is broken, that means we failed.
        */
        disconnect();
        return false;
    }

    /**
     * Checks that OpenVPN is running and that the network is reachable.
     * @return
     */
    public boolean isAlive() {
        return isRunning() && checkConnectivity();
    }
    
    /**
     *
     * @return
     */
    public boolean isRunning(){
        return openVPNProcess != null && openVPNProcess.isAlive();
    }

    /**
     * Disconnect from the VPN.
     */
    public void disconnect() {
        killOpenVPN();
        setNetworkInterface(null);
    }

    /**
     * 
     */
    private void killOpenVPN() {
        if (openVPNProcess == null) {
            return;
        }

        //Checks wether the process is alive
        if (openVPNProcess.isAlive()) {
            //Kindly send a termination signal to the process
            openVPNProcess.destroy();
            try {
                //Let some time for the process to exit
                if (!openVPNProcess.waitFor(10, TimeUnit.SECONDS)) {
                    //If the process is still alive, kick his ass
                    Logger.getLogger(VPN.class.getName()).log(Level.WARNING, "OpenVPN stopped forcibly after timeout");
                    //openVPNProcess.destroyForcibly();
                    openVPNProcess.destroy();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(VPN.class.getName()).log(Level.WARNING, "Interrupted while waiting for OpenVPN to stop. Stopping it forcibly", ex);
                openVPNProcess.destroyForcibly();
            }
        }

        //Checking the return value
        if (openVPNProcess.exitValue() != 0) {
            Logger.getLogger(VPN.class.getName()).log(Level.WARNING, "OpenVPN did not stop correctly");
        }
        
        openVPNProcess = null;
    }

    /**
     * This function checks that the given configuration file exists, has a valid
     * network interface specified (\\b(tun[0-9]+)\\b) and log a warning if the
     * interface already exists
     * @return The name of the network interface according to the configuration file, null on error.
     */
    private String checkNicConfiguration() {
        FileReader fr;
        String dev = null;
        try {
            fr = new FileReader(configPath);
            //Checking that the network interface doesn't already exists
            try (BufferedReader reader = new BufferedReader(fr)) {
                //Checking that the network interface doesn't already exists
                Pattern tunPattern = Pattern.compile("\\b(tun[0-9]+)\\b", Pattern.MULTILINE);
                String line;
                
                while ((line = reader.readLine()) != null) {
                    Matcher matcher = tunPattern.matcher(line);
                    if (matcher.find()) {
                        dev = matcher.group(1);
                        
                        if (NetworkInterface.getByName(dev) != null) {
                            LOGGER.log(Level.SEVERE, "Network interface {0} already exists.", dev);
                        }
                    }
                }
                if (dev == null) {
                    LOGGER.log(Level.SEVERE, "No network interface found on configuration file.");
                }
            }
            fr.close();
        } catch (IOException ex) {
            Logger.getLogger(VPN.class.getName()).log(Level.SEVERE, "Configuration file " + configPath + " not found or unreadable.", ex);
        }

        return dev;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString(){
        return this.name; //It is possible to get better : printing the state of the VPN.
    }
}
