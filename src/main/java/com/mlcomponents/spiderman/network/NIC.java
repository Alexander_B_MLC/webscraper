
package com.mlcomponents.spiderman.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.util.Enumeration;

/**
 * Network Interface Card.
 * Just a set of tools for Java's NetworkInterface.
 * @author antonin
 */
public class NIC {
    private NetworkInterface nic;
    
    /**
     *
     */
    public NIC(){
        this.nic = null;
    }
    
    /**
     * Constructor.
     * @param nic The network interface to manage. This should not be null.
     */
    public NIC(NetworkInterface nic){
        this.nic = nic;
    }
    
    /**
     * Gets an address that is not link local if it exists
     * @return the address on success, null otherwise
     */
    public InetAddress getAddress(){
        if(this.nic == null) return null;
        Enumeration<InetAddress> inets = nic.getInetAddresses();
        
        //We search for the first valid address
        while(inets.hasMoreElements()){
                InetAddress inet = inets.nextElement();
                if(!inet.isLinkLocalAddress() && !inet.getHostAddress().contains(":")) //We don't want a link local address (fe80::)
                    return inet;
        }
        
        return null;
    }
    
    /**
     * TODO
     * @return
     */
    public boolean checkConnectivity(){
        InetAddress addr = getAddress();
        if(addr == null) return false;

        //System.out.println("Address : "+addr.getHostAddress());
        try (Socket sock = new Socket()) {
            sock.bind(new InetSocketAddress(addr, 0));
            sock.connect(new InetSocketAddress(InetAddress.getByName("www.google.fr"), 80));
            return true;
        }catch(IOException e){
            return false; // Either timeout or unreachable or failed DNS lookup.
        }
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString(){
        InetAddress addr = getAddress();
        return nic.getName() + ", " + ((addr == null)?"no address":(addr.getHostAddress()));
    }
    
    /**
     *
     * @param networkInterface
     */
    public void setNetworkInterface(NetworkInterface networkInterface){
        this.nic = networkInterface;
    }
}
