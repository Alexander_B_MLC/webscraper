
package com.mlcomponents.spiderman.network;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class manage a set of VPNs.
 * @author antonin
 */
public class VPNManager extends NetworkManager {
    private static final Logger LOGGER = Logger.getLogger(VPNManager.class.getName());
    private static final AtomicInteger connectedVPNs = new AtomicInteger(0);
    private static final int MAX_VPN_CONNECTIONS = 5;

    /**
     *
     * @param configPath
     * @throws IOException
     */
    public VPNManager(String configPath) throws IOException{
        addVPN(configPath);
    }
    
    /**
     *
     * @param configPath
     * @throws IOException
     */
    public final void addVPN(String configPath) throws IOException{
        File configFile = new File(configPath);
        if(!configFile.exists()){
            throw new IOException("Config path "+configPath+" doesn't exists");
        }else{
            if(configFile.isFile()){
                addVPNFromFile(configFile);
            }else if(configFile.isDirectory()){
                File[] listFiles = configFile.listFiles();
                for(File f : listFiles){
                    addVPNFromFile(f);
                }
            }
        }
    }
    
    /**
     * Adds a VPN from a configFile.
     * Its name is the one of its configuration file, without its extension.
     * @param configFile
     * @throws IOException 
     */
    private void addVPNFromFile(File configFile) throws IOException {
        if(!configFile.exists() || !configFile.isFile()){
            throw new IOException(configFile+" is not a file or doesn't exists.");
        }else{
            String name = configFile.getName();
            int indexExt;
            if((indexExt = name.lastIndexOf(".ovpn")) != -1){
                name = name.substring(0, indexExt);
            }else{return;}
            if(!addInterface(name,  new VPN(name, configFile.getPath()))){
                LOGGER.log(Level.WARNING, "VPN {0} was existing and has been replaced.", name);
            }
        }
    }
    
    /**
     * Connect to a VPN with its name.
     * @param name The name of the VPN to connect.
     * @return True if the connection was successful, false otherwise.
     */
    public boolean connect(String name){
        if(connectedVPNs.get() >= MAX_VPN_CONNECTIONS) return false;
        VPN vpn = (VPN) getInterface(name);
        if(vpn == null){
            LOGGER.log(Level.WARNING, "VPN {0} doesn't exist.", name);
            return false;
        }
        
        boolean result = vpn.connect();
        if(result){
            connectedVPNs.incrementAndGet();
        }
        return result;
    }
    
    /**
     * Disconnect from a VPN with its name.
     * @param name The name of the VPN.
     */
    public void disconnect(String name){
        if(connectedVPNs.get() <= 0) return;
        VPN vpn = (VPN) getInterface(name);
        if(vpn == null){
            LOGGER.log(Level.WARNING, "VPN {0} doesn''t exist.", name);
            return;
        }
        
        vpn.disconnect();
        connectedVPNs.decrementAndGet();
    }
    
    /**
     *
     * @return
     */
    public boolean canAddConnection(){
        return connectedVPNs.get() < MAX_VPN_CONNECTIONS;
    }
    
    /**
     *
     * @param name
     * @return
     */
    public boolean isRunning(String name){
        VPN vpn = (VPN) getInterface(name);
        return (vpn != null && vpn.isRunning());
    }
    
}
