
package com.mlcomponents.spiderman.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;

/**
 *
 * @author antonin
 */
public interface HttpGetInterface {

    /**
     *
     * @param uri
     * @param localAddress
     * @return
     * @throws IOException
     */
    public String getText(URI uri, InetAddress localAddress) throws IOException;

    /**
     *
     * @param uri
     * @param localAddress
     * @return
     * @throws IOException
     */
    public byte[] getByteArray(URI uri, InetAddress localAddress) throws IOException;
}
