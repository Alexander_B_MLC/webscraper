
package com.mlcomponents.spiderman.network;

import com.mlcomponents.spiderman.documents.AbstractBinaryDocument;
import com.mlcomponents.spiderman.documents.AbstractDocument;
import com.mlcomponents.spiderman.documents.AbstractTextualDocument;
import com.mlcomponents.spiderman.network.RequestsManager.RequestToken;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.Date;

/**
 * This class is an utility for threads to download content.
 * It handles the interactions with the request manager and the HTTP client.
 * It can be used to store thread's specific network configuration f.e.g.
 * @author antonin
 */
public class DownloadManager {
    private RequestsManager requestsManager;
    private HttpHandler httpHandler;

    /**
     *
     * @param requestsManager
     * @param httpHandler
     */
    public DownloadManager(RequestsManager requestsManager, HttpHandler httpHandler) {
        if(requestsManager == null) throw new IllegalArgumentException("Request manager must not be null");
        if(httpHandler == null) throw new IllegalArgumentException("HTTP handler must not be null");
        this.requestsManager = requestsManager;
        this.httpHandler = httpHandler;
    }

    /**
     *
     * @param uri
     * @return
     * @throws IOException
     */
    public String getText(URI uri) throws IOException {
        RequestToken token = requestsManager.acquire(uri.getHost());
        System.out.println(token.getName()+" - "+token.getAddress());
	try{
		String text = httpHandler.getText(uri, token.getAddress());
		token.release();
		return text;
		
	}catch(SocketTimeoutException e){
		if(token.valid){
		token.release();
		
		}
	throw e;
	}
	
    }

    /**
     *
     * @param uri
     * @return
     * @throws IOException
     */
    public byte[] getByteArray(URI uri) throws IOException {
        RequestToken token = requestsManager.acquire(uri.getHost());
        byte[] bytes = httpHandler.getByteArray(uri, token.getAddress());
        token.release();
        return bytes;
    }
    
    /**
     *
     * @param document
     * @param uri
     * @throws IOException
     */
    public void downloadDocument(AbstractDocument document, URI uri) throws IOException{
        if(document instanceof AbstractTextualDocument){
            ((AbstractTextualDocument) document).setData(getText(uri));
        }else if(document instanceof AbstractBinaryDocument) {
            ((AbstractBinaryDocument) document).setData(getByteArray(uri));
        }else{
            throw new IllegalArgumentException("Not supported yet");
        }
        document.setDownloadDate(new Date(System.currentTimeMillis()));
        document.saveToFile();
    }
}
