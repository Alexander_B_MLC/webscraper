
package com.mlcomponents.spiderman.network;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

/**
 * This class represents a NetworkInterface, that can be used to connect to the internet.
 * @author antonin
 */
public class NetworkManager {
    
    private final HashMap<String, NIC> ifaces;
    
    /**
     *
     */
    public NetworkManager(){
        this.ifaces = new HashMap<>();
    }
    
    /**
     *
     * @param excludeLoopback
     * @param excludeP2P
     * @param onlyUp
     * @param onlyVirtual
     * @return
     * @throws SocketException
     */
    public static ArrayList<NetworkInterface> scanInterfaces(boolean excludeLoopback, boolean excludeP2P, boolean onlyUp, boolean onlyVirtual) throws SocketException{
        //Look for network interfaces
        ArrayList<NetworkInterface> networkInterfaces = new ArrayList<>();
        Enumeration<NetworkInterface> nicsEnum = NetworkInterface.getNetworkInterfaces();

        //Keep track of useful interfaces
        while(nicsEnum.hasMoreElements()){
            NetworkInterface nic = nicsEnum.nextElement();
            if((!excludeLoopback || !nic.isLoopback())
                && (!excludeP2P || !nic.isPointToPoint())
                && (!onlyUp || nic.isUp())
                && (!onlyVirtual || nic.isVirtual())){
                networkInterfaces.add(nic);
            }
        }
        
        return networkInterfaces;
    }   
    
    /**
     *
     * @param name
     * @param iface
     * @return
     */
    public boolean addInterface(String name, NIC iface){
        NIC previous = this.ifaces.put(name, iface);
        return previous==null;
    }
    
    /**
     *
     * @param name
     */
    public void removeInterface(String name){
        this.ifaces.remove(name);
    }
    
    /**
     *
     * @param name
     * @return
     */
    public NIC getInterface(String name){
        return ifaces.get(name);
    }
    
    /**
     *
     * @return
     */
    public ArrayList<String> getInterfacesNames(){
        return new ArrayList<>(ifaces.keySet());
    }

    /**
     *
     * @return
     */
    public Object getInterface() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
