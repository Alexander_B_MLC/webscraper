
package com.mlcomponents.spiderman.network;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mlcomponents.spiderman.tools.DelayGenerator;
import java.net.InetAddress;

/**
 *
 * @author antonin
 */
public class RequestsManager {
    
    private static final Logger LOGGER = Logger.getLogger(VPN.class.getName());
    
    private final int HOST_DELAY_LOWER_BOUND = 5000;//ms
    private final int HOST_DELAY_UPPER_BOUND = 10000;
    
    private final int VPN_HOST_DELAY_LOWER_BOUND = 300000;//ms
    private final int VPN_HOST_DELAY_UPPER_BOUND = 600000;
    
    private final int DISCONNECT_DELAY_LOWER_BOUND = 60000;//ms
    private final int DISCONNECT_DELAY_UPPER_BOUND = 120000;
    
    private VPNManager vpnManager;
    
    private HashSet<String> busyHosts;
    private HashSet<String> busyVPNs;
    private HashMap<String, HashSet<String>> blacklistedVPNsForHost;
    private HashMap<String, HashSet<String>> busyVPNsForHost;
    private ArrayList<String> canExpireVPNs;
    
    private HashMap<String, Condition> hostReadyMap;
    private Condition vpnUpdate;
    
    private final Lock monitor = new ReentrantLock(true);
    
    /**
     *
     * @param vpnManager
     */
    public RequestsManager(VPNManager vpnManager){
        if(vpnManager == null) throw new IllegalArgumentException("vpnManager must not be null");
        this.vpnManager = vpnManager;
        
        busyHosts = new HashSet<>();
        busyVPNs = new HashSet<>();
        hostReadyMap = new HashMap<>();
        blacklistedVPNsForHost = new HashMap<>();
        busyVPNsForHost = new HashMap<>();
        vpnUpdate = monitor.newCondition();
        canExpireVPNs = new ArrayList<>();
    }
    
    /**
     *
     * @param host
     * @return
     */
    public RequestToken acquire(String host){
        String electedVPN = null;
        monitor.lock();
        try {
            //Checking if the host is not delayed
            while(busyHosts.contains(host)){
                try {
                    hostReadyMap.get(host).await();
                } catch (InterruptedException ex) {
                    Logger.getLogger(RequestsManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } finally {
            //Getting the host lock
            busyHosts.add(host);
            if(!hostReadyMap.containsKey(host)){
                hostReadyMap.put(host, monitor.newCondition());
            }
            
            do{
                //Get a list of VPNs ready for the host
                ArrayList<String> vpns = vpnManager.getInterfacesNames();
                ArrayList<String> readyVPNs = new ArrayList<>();

                readyVPNs.clear();
                vpns.forEach((String name) -> {
                    if(!busyVPNs.contains(name)){//The VPN is not busy
                        HashSet<String> busyForHost = busyVPNsForHost.get(host);
                        HashSet<String> blacklistedVPNs = blacklistedVPNsForHost.get(host);
                        if(
                                (blacklistedVPNs == null || !blacklistedVPNs.contains(name))
                                && (busyForHost == null || !busyForHost.contains(name))){//It can connect to the host
                            readyVPNs.add(name);
                        }
                    }
                });
                if(readyVPNs.isEmpty()){
                    try {
                        vpnUpdate.await();
                        continue;
                    } catch (InterruptedException ex) {
                        Logger.getLogger(RequestsManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                Collections.shuffle(readyVPNs);
                
                Iterator<String> it = readyVPNs.iterator();
                while(electedVPN == null && it.hasNext()){
                    String next = it.next();
                    if(vpnManager.isRunning(next)
                       || (vpnManager.canAddConnection() && connectVPN(next))){
                        electedVPN = next;
                    }
                    else if(!canExpireVPNs.isEmpty()){
                        vpnManager.disconnect(canExpireVPNs.get(0));
                        canExpireVPNs.remove(0);
                        if(connectVPN(next)){electedVPN = next;}
                    }
                }
                
                if(electedVPN == null){
                    try {
                        vpnUpdate.await();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(RequestsManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }while(electedVPN == null);//TO CHANGE
            
            //Get the lock for the VPN
            busyVPNs.add(electedVPN);
            
            //Get the lock for the VPN, for a specific host
            HashSet<String> busyVPNsHostSet = busyVPNsForHost.get(host);
            if(busyVPNsHostSet == null){
                busyVPNsHostSet = new HashSet<>();
                busyVPNsForHost.put(host, busyVPNsHostSet);
            }
            busyVPNsHostSet.add(electedVPN);
            
            monitor.unlock();
        }
        return new RequestToken(electedVPN, host, vpnManager.getInterface(electedVPN).getAddress());
    }
    
    private boolean connectVPN(String vpn){
        boolean result = vpnManager.connect(vpn);
        if(result){
            setDisconnectDelay(vpn);
        }
        return result;
    }
    
    /**
     * Blacklist a VPN for a specific host
     * @param vpnName The VPN to blacklist
     * @param hostName The host name
     */
    public void blacklistVPNForHost(String vpnName, String hostName){
        HashSet<String> blacklistedVPNs = blacklistedVPNsForHost.get(hostName);
        if(blacklistedVPNs == null){
            blacklistedVPNs = new HashSet<>();
            blacklistedVPNsForHost.put(hostName, blacklistedVPNs);
        }
        blacklistedVPNs.add(vpnName);
    }
    
    private void release(String vpn, String host){
        monitor.lock();
        try {
            setHostDelay(host);
            busyVPNs.remove(vpn);
            setVPNHostDelay(vpn, host);
        } finally {
            monitor.unlock();
        }
    }
    
    private void setHostDelay(String host){
        Timer t = new Timer();
        long delay = DelayGenerator.generateDelay(HOST_DELAY_LOWER_BOUND, HOST_DELAY_UPPER_BOUND);
        t.schedule(new SetHostReadyTask(host), delay);
        LOGGER.log(Level.INFO, "{0} delayed for {1} s", new Object[]{host, String.valueOf(delay/1000.)});
    }
    
    private void setHostReady(String host){
        monitor.lock();
        try {
            busyHosts.remove(host);
            hostReadyMap.get(host).signal();
        } finally {
            monitor.unlock();
        }
    }
    
    private class SetHostReadyTask extends TimerTask {
        private final String host;
        
        SetHostReadyTask(String host){
            this.host = host;
        }
        
        @Override
        public void run() {
            RequestsManager.this.setHostReady(this.host);
        }
    }
    
    private void setDisconnectDelay(String vpn){
        Timer t = new Timer();
        long delay = DelayGenerator.generateDelay(DISCONNECT_DELAY_LOWER_BOUND, DISCONNECT_DELAY_UPPER_BOUND);
        t.schedule(new SetDisconnectReadyTask(vpn), delay);
        LOGGER.log(Level.INFO, "{0} disconnection delayed for {1} s", new Object[]{vpn, String.valueOf(delay/1000.)});
    }
    
    private void setDisconnectReady(String vpn){
        monitor.lock();
        try {
            canExpireVPNs.add(vpn);
            vpnUpdate.signalAll();
        } finally {
            monitor.unlock();
        }
    }
    
    private class SetDisconnectReadyTask extends TimerTask {
        private final String VPN;
        
        SetDisconnectReadyTask(String vpn){
            this.VPN = vpn;
        }
        
        @Override
        public void run() {
            RequestsManager.this.setDisconnectReady(this.VPN);
        }
    }
    
    private void setVPNHostDelay(String vpn, String host){
        Timer t = new Timer();
        long delay = DelayGenerator.generateDelay(VPN_HOST_DELAY_LOWER_BOUND, VPN_HOST_DELAY_UPPER_BOUND);
        t.schedule(new SetVPNReadyForHostTask(vpn, host), delay);
        LOGGER.log(Level.INFO, "VPN {0} delayed for {1} s for host {2}", new Object[]{vpn, String.valueOf(delay/1000.), host});
    }
    
    private void setVPNReadyForHost(String vpn, String host){
        monitor.lock();
        try {
            HashSet<String> busyVPNsForTheHost = busyVPNsForHost.get(host);
            if(busyVPNsForTheHost != null){
                busyVPNsForTheHost.remove(vpn);
            }
        } finally {
            vpnUpdate.signalAll();
            monitor.unlock();
        }
    }
    
    private class SetVPNReadyForHostTask extends TimerTask {
        private final String VPN;
        private final String HOST;
        
        SetVPNReadyForHostTask(String vpn, String host){
            this.VPN = vpn;
            this.HOST = host;
        }
        
        @Override
        public void run() {
            RequestsManager.this.setVPNReadyForHost(VPN, HOST);
        }
    }
    
    /**
     *
     */
    public class RequestToken{
        private final String interfaceName;
        private final String hostName;
        private final InetAddress address;
        public boolean valid = true;

        private RequestToken(String interfaceName, String hostName, InetAddress address) {
            this.interfaceName = interfaceName;
            this.hostName = hostName;
            this.address = address;
        }
        
        /**
         *
         * @return
         */
        public InetAddress getAddress(){
            return valid?address:null;
        }
        
        /**
         *
         */
        public void release(){
            valid = false;
            RequestsManager.this.release(interfaceName, hostName);
        }
        
        /**
         *
         * @return
         */
        public String getName(){
            return interfaceName;
        }
        
    }
}
