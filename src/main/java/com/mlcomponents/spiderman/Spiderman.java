package com.mlcomponents.spiderman;

import com.mlcomponents.spiderman.network.DownloadManager;
import com.mlcomponents.spiderman.network.HttpHandler;
import com.mlcomponents.spiderman.network.RequestsManager;
import com.mlcomponents.spiderman.network.VPNManager;
import com.mlcomponents.spiderman.core.SpiderLauncher;
import com.mlcomponents.spiderman.targets.digikey.Digikey;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * This is the starting point of the program.
 *
 * @author antonin
 */
public class Spiderman {

    private static final Logger LOGGER = Logger.getLogger(Spiderman.class.getName());
    private static final String VPN_CONFIGURATION_PATH = "./openvpn/";
    
    /**
     *
     * @param args
     * @throws KeyStoreException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @throws URISyntaxException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException, IOException, URISyntaxException, ClassNotFoundException, InterruptedException {

        //Set logger level
        Logger rootLogger = LogManager.getLogManager().getLogger("");
        rootLogger.setLevel(Level.WARNING);
        for (Handler h : rootLogger.getHandlers()) {
            h.setLevel(Level.WARNING);
        }
        
        System.out.println("HAI");

        /** 
         * Setup network management objects.
         * These objects must be unique and shared between the different instances of the scraper.
         */
        VPNManager vpnMgr = new VPNManager(VPN_CONFIGURATION_PATH);
        RequestsManager requestsManager = new RequestsManager(vpnMgr);
        HttpHandler httpHandler = new HttpHandler();
        DownloadManager downloadManager = new DownloadManager(requestsManager, httpHandler);
        
        /**
         * Configure the target.
         * Note that the configuration is static : we don't really need to dynamically set this,
         * and having static configuration reduce a lot the need of dependency injection.
         */
        Digikey.DIGIKEY_LINK_ALL_PRODUCTS_BUILDER
                .setDatasheetFilter(true)
                .setPhotoFilter(true)
                .setRoHSCompliantFilter(true);
        Digikey.DIGIKEY_LINK_PRODUCTS_BUILDER
                .setDatasheetFilter(true)
                .setPhotoFilter(true)
                .setRoHSCompliantFilter(true);
        /**
         * WARNING : If you change the configuration, and then resume a scraping task
         * made for another configuration, the scraper will not adapt the previously scraped data.
         */

        Digikey target = new Digikey();
        System.out.println(target.getName());
        
        //Start the scraper
        SpiderLauncher launcher = new SpiderLauncher(target, downloadManager);
        launcher.start();
        
        /**
         * Note : This program is multi-threaded. That means you can create an
         * instance for another target, and run it (with the same downloadManager).
         * Example :
         * OtherSite otherTarget = new OtherSite();
         * SpiderLauncher otherLauncher = new SpiderLauncher(target, downloadManager);
         * otherLauncher.start();
         */
        
        //When all the scrapers are launched, join them
        launcher.join();
        //otherLauncher.join();

        System.out.println("KTHXBYE");
    }
}
