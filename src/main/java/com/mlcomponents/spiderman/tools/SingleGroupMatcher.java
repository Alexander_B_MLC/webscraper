/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlcomponents.spiderman.tools;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author antonin
 */
public class SingleGroupMatcher{
    private final Pattern pattern;

    /**
     *
     * @param regexp
     */
    public SingleGroupMatcher(String regexp) {
        this.pattern = Pattern.compile(regexp);
    }
    
    /**
     *
     * @param in
     * @return
     */
    public String getSingleGroup(String in){
        Matcher m= pattern.matcher(in);
        if(!m.find()) return null;
        if(m.groupCount() < 1) return null;
        return m.group(1);
    }
}
