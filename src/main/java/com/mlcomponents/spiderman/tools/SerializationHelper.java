
package com.mlcomponents.spiderman.tools;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * https://www.codementor.io/java/tutorial/serialization-and-deserialization-in-java
 * @author Aparajita Jain
 */
public class SerializationHelper {

    /**
     *
     * @param file
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Object deSerialization(String file) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        Object object;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(bufferedInputStream)) {
            object = objectInputStream.readObject();
        }
        return object;
    }

    /**
     *
     * @param file
     * @param object
     * @throws IOException
     */
    public static void serialization(String file, Object object) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(bufferedOutputStream)) {
            objectOutputStream.writeObject(object);
        }
    }
}
