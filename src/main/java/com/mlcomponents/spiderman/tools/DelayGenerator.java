/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlcomponents.spiderman.tools;

import java.util.Random;

/**
 *
 * @author antonin
 */
public class DelayGenerator {
    private static Random randomGenerator = new Random();
    
    /**
     *
     * @param lowerBound
     * @param upperBound
     * @return
     */
    public static long generateDelay(int lowerBound, int upperBound){
        return (randomGenerator.nextInt(upperBound-lowerBound) + lowerBound);
    }
}
