


import com.mlcomponents.spiderman.network.RequestsManager;
import com.mlcomponents.spiderman.network.VPNManager;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.mockito.Mockito;



/**
 * This class launch multiple threads to test network concurrency management.
 * @author antonin
 */
public class SpiderThreadLauncher {

    /**
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException{
        try {
            //We don't care about the VPN manager for the moment
            //RequestsManager reqMgr = new RequestsManager(Mockito.mock(VPNManager.class));
            RequestsManager reqMgr = new RequestsManager(new VPNManager("./openvpn"));
            
            
            ArrayList<URL> urls = new ArrayList<>();
            urls.add(new URL("https://bing.com"));
            urls.add(new URL("https://mlcomponents.com"));
            urls.add(new URL("https://bdelectronics.com"));
            
            Thread thread0 = new Thread(new SpiderThreadTest(reqMgr, urls));
            Thread thread1 = new Thread(new SpiderThreadTest(reqMgr, urls));
            Thread thread2 = new Thread(new SpiderThreadTest(reqMgr, urls));
            
            thread0.start();
            thread1.start();
            thread2.start();
        } catch (MalformedURLException ex) {
            Logger.getLogger(SpiderThreadLauncher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
