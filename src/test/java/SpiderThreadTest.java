


import com.mlcomponents.spiderman.network.RequestsManager;
import com.mlcomponents.spiderman.network.RequestsManager.RequestToken;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class implements an example of a very basic spider that wants to make
 * <p>connections to an host.
 * @author antonin
 */
public class SpiderThreadTest implements Runnable {
    private static int instancesCount = 0;
    private int instanceNumber;
    private ArrayList<String> hosts;
    private final RequestsManager mgr;
    
    private SpiderThreadTest(RequestsManager mgr){
        this.instanceNumber = instancesCount++;
        this.hosts = new ArrayList<>();
        this.mgr = mgr;
    }
    
    /**
     *
     * @param mgr
     * @param urls
     */
    public SpiderThreadTest(RequestsManager mgr, ArrayList<URL> urls){
        this(mgr);
        urls.forEach((URL url) -> this.hosts.add(url.getHost()));
    }
    
    /**
     *
     * @param mgr
     * @param url
     */
    public SpiderThreadTest(RequestsManager mgr, URL url){
        this(mgr);
        hosts.add(url.getHost());
    }
    
    /**
     *
     */
    @Override
    public void run() {
        System.out.println("Hi, i'm instance "+instanceNumber+" "+hosts);
        while(true){
            hosts.forEach((String host) -> {
                System.out.println(instanceNumber + ": trying to reach "+host);
                RequestToken token = mgr.acquire(host);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SpiderThreadTest.class.getName()).log(Level.SEVERE, null, ex);
                }
                token.release();
                System.out.println(instanceNumber + ": success ("+token.getName()+")");
            });
            
        }
    }
    
}
