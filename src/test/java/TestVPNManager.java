


import com.mlcomponents.spiderman.network.VPN;
import com.mlcomponents.spiderman.network.VPNManager;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author antonin
 */
public class TestVPNManager {

    /**
     *
     * @param args
     */
    public static void main(String[] args){
        try {
            final VPNManager vpnMgr = new VPNManager("./openvpn/");;
            
            try {
                System.out.println("Scan interface test : "+VPNManager.scanInterfaces(false, false, false, false));
            } catch (SocketException ex) {
                Logger.getLogger(TestVPNManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.out.println("Test interface names : ");
            ArrayList<String> names = vpnMgr.getInterfacesNames();
            System.out.println(names);
            
            System.out.println("Connection to 5 random VPNs");
            Collections.shuffle(names);
            for(int i = 0; i < 5; i++){
                if(vpnMgr.connect(names.get(i)) && ((VPN)vpnMgr.getInterface(names.get(i))).isAlive()){
                    System.out.println(names.get(i)+" success");
                }else{
                    System.out.println(names.get(i)+" fail");
                }
            }
            
            System.out.println("Disconnecting all");
            names.forEach((String name) -> {vpnMgr.disconnect(name);});
        } catch (IOException ex) {
            Logger.getLogger(TestVPNManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
